MD Converter
===

I'm specific about markdown style!

<!-- ![1](media/***.jpg) -->


## Description

To make markdown converter being specifically for me.


## Dependency

```bash
pip install markdown
pip install markdown2
```


## Usage

```python
import MdConverter

# MD file to HTML file
MdConverter.convert_md_file(md_path, html_path)

# Get HTML string from MD string
html = MdConverter.convert_md(md)
```
