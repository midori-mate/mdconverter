
import nose
from nose.tools import ok_, eq_
from parameterized import parameterized

import MdConverter

# テスト内容。 (markdown, expected_html)
TEST_CASES = [
# fenced_code の実験。
('''
```
fenced_code
```
''',
'<pre><code>fenced_code</code></pre>',),
# table の実験。
('''
|  title  |  title  |
|---------|---------|
| content | content |
''',
'<table><thead><tr><th>title</th><th>title</th></tr></thead><tbody><tr><td>content</td><td>content</td></tr></tbody></table>',),
# dl dt dd の実験。
('''
DT
:   DD
''',
'<dl><dt>DT</dt><dd>DD</dd></dl>',),
# リストの実験。
('''
- ひとつめ
- ふたつめ

1. ひとつめ
1. ふたつめ
''',
'<ul><li>ひとつめ</li><li>ふたつめ</li></ul><ol><li>ひとつめ</li><li>ふたつめ</li></ol>',),
# attr_list の実験。
('''
test-text
{.color-red}
''',
'<p class="color-red">test-text</p>',),
]

@parameterized(TEST_CASES)
def test(md, expected, expect_exception=False):

    # 通常の返り値期待。
    if not expect_exception:
        # テストケースを書きやすくするため、改行は度外視(排除)します。
        actual = MdConverter.convert_md(md)
        actual = (actual.replace('\r','')).replace('\n','')
        eq_(expected, actual)
        return

    # 例外期待。
    try:
        MdConverter.convert_md(md)
    except Exception as e:
        ok_(isinstance(e, expected))
        return
    ok_(True is False, 'Exception must be thrown.')


if __name__ == '__main__':
    # なんか知らんけどテストモジュールが executable だっつって実行しねえからオプションが必要。
    nose.main(
        argv=['nosetests', '-v', '-s', '--exe']
    )
