
"""
MdConverter の動きを見るだけのスクリプトだよ ミ★
"""

import MdConverter

# MDを2種類用意。
md1 = '''Title: TestMd1
Author: Me
Salvia Tree Tulip Tree
'''
md2 = '''Title: TestMd2
Author: You
Cleaning staff in the zoo
'''

md_instance1, html_string1 = MdConverter.create_md_instance(md1, MdConverter.EXTENSIONS_ONE)
md_instance2, html_string2 = MdConverter.create_md_instance(md2, MdConverter.EXTENSIONS_ONE)

print(md_instance1.Meta)
print(html_string1)
print(md_instance2.Meta)
print(html_string2)
