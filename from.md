Title: Test MD document
Author: Midori

Python-Markdown Note
===


## Installation

```bash
# ふつうに pip でインストール。
pip install markdown
```


## Usage

```python
import markdown
print(markdown.markdown(md_text, extensions=[]))
```


## デフォルトじゃできないこと

### コード

extensions に `fenced_code` を追加。 (2 では `fenced-code-blocks`)

    ```python
    てかデフォルトでこれができねーのがビックリなんだよ。
    ```

### テーブル

extensions に `tables` を追加。 (2 でも同名。)

|  Table   | は | ぜひとも |
|----------|----|----------|
| 使いたい   | よ | ね       |

### dl dt dd

extensions に `def_list` を追加。 Sublimeちゃんの Omni では出ないっぽいけど。

DT
:   DL

### デフォルトだとリストがバカなのでなんとかする

extensions に `sane_lists` を追加。 (2 では必要ない。)

- ひとつめ
- ふたつめ

1. ひとつめ
1. ふたつめ

### 目次

extensions に `toc` を追加。 これはシンプルなMDという観点からは余計な気がするけど、これを自動で生成してくれるのはよくね?

[TOC]

### 文字強調

extensions に `markdown.extensions.smart_strong` を追加。 へんなふうにいくつも`**`を重ねたりしたときのエラーを防げるっぽい。 念のために入れておこうか。

**太文字**、__太文字__。

### 脚注

extensions に `footnotes` を追加。

まあ別にそんなに使わない[^1]けど使い方だけ覚えておこうか。[^foobar]

[^1]: ひとつめの脚注。
[^foobar]: ふたつめの脚注。

### CSS とかは MD の中で普通に読み込める

<link rel="stylesheet" href="./css.css">

色を緑にしてみる。
{: .color-green}

これを使えば、画像を複数記事に渡って使いたいって要望が解決するかも。(`background-image` とかで)

### クラスをMDに書けるように

extensions に `attr_list` を追加。 これは世界が広がるぞ。 すごい。 style をMD内にふつうに書けるのもいい。

<style>
    .color-red {
        color: red;
    }
    .black-dot {
        display: block;
        background: black;
        border-radius: 50%;
        width:10px;
        height:10px;
        margin-left:15px;
    }
</style>

色を赤くしてみる。
{.color-red}

ついでにおなじみの黒丸を出してみよう。  

#### 　 #### {.black-dot}

● (本物)

文章の*最中*{.color-red}に style を表示してみる。 何の意味もない`* *`にこんな使い方があったとは…。

```python
# 背景色ない版
# 背景色ない版
# 背景色ない版
```

```python
# 背景色ある版
# 背景色ある版
# 背景色ある版
```
{: .bg-light-color}

attr_list が fenced_code に適用されねえ!!
