
"""MrrhpUploader
Upload drafts onto Mrrhp based on Markdown file.

"""

import argparse
import os
import markdown
import datetime
import sqlite3
import contextlib


# TODO: 各定数をまとめておく。
# TODO: .md と db の id 仕様を決める。


# 引数を受け取ります。 ArgumentParser
# TODO: --md --html の反映がまだ。
parser = argparse.ArgumentParser(description='MrrhpUploader')
parser.add_argument('dir', type=str, help='Name of draft dir.')
parser.add_argument('--md', action='store_true', help='Just execute md conversion.')
parser.add_argument('--html', action='store_true', help='Just execute html uploading.')
args = parser.parse_args()

print(args)


# 定数。
DRAFT_DIR = f'./drafts/{args.dir}'
HTML_FILE = f'{DRAFT_DIR}/.html'
MD_FILE = f'{DRAFT_DIR}/.md'


# 草稿ディレクトリ存在確認。
if not os.path.exists(DRAFT_DIR):
    os.mkdir(DRAFT_DIR)


# .html 存在確認。
if not os.path.isfile(HTML_FILE):
    with open(HTML_FILE, 'w', encoding='utf8') as f:
        pass


# .md 存在確認。
if not os.path.isfile(MD_FILE):
    with open(MD_FILE, 'w', encoding='utf8') as f:
        pass


# .md テキスト取得。
with open(MD_FILE, 'r', encoding='utf8') as f:
    md_string = f.read()


# md -> html 変換と Meta 取得。
EXTENSIONS = [
    'fenced_code',  # ``` のコードを書ける。
    'tables',       # table を書ける。
    'def_list',     # dl dt dd を書ける。
    # 'nl2br',      # ふつーに改行すれば改行される。 まあこれはいらんかな。
    'sane_lists',   # デフォルトだとリストがバカだからなんとかする。
    'toc',          # [TOC] で目次自動生成。
    'attr_list',    # 要素に id とか class を追加できる。
    'markdown.extensions.smart_strong', # 強調がすこしだけバカだからなんとかする。
    'footnotes',    # 脚注が使えるようになる。
    'meta',         # メタ情報を MD 内に書ける。
]
md_instance = markdown.Markdown(extensions=EXTENSIONS)
html_string = md_instance.convert(md_string)


# Meta の整理。
md_meta = {}
for key,value in md_instance.Meta.items():
    if len(value) == 1:
        md_meta[key] = value[0]
    else:
        md_meta[key] = value

print(md_meta)
print(html_string)


# Meta の必須項目チェック。
REQUIRED_META_KEYS = [
    'publishdate',
    'published',
]
for key in REQUIRED_META_KEYS:
    if key not in md_meta:
        raise ValueError(f'Your MD file does not have meta key: {key}')


# Meta によっては html を修正。
# TODO: 拡張が容易なように作成する。
for command in md_meta['htmlcommands']:
    if command == 'p-class-test':
        html_string = html_string.replace('<p>', '<p class="test">')
    elif command == 'black-dot':
        html_string = html_string.replace('くろまる', '●')

print(html_string)


# .html に記述。
with open(HTML_FILE, 'w', encoding='utf8') as f:
    f.write(html_string)


# db 処理。
# TODO: とりあえず :memory: に作って実験してる。
with contextlib.closing(sqlite3.connect(':memory:')) as con:
    con.row_factory = sqlite3.Row

    # テーブル作成。
    con.execute('CREATE TABLE post(id, html, publish_date, published)')

    # db 存在確認。
    rows = []
    for row in con.execute('SELECT 0 FROM post WHERE id=:id', {'id':md_meta['publishdate']}):
        rows.append(row)
    exists = bool(rows)

    # db へ記録。
    if exists:
        sql = ' '.join([
            'UPDATE post SET',
                'html=:html,',
                'publish_date=:publish_date,',
                'published=:published',
            'WHERE id=:id',
        ])
    else:
        sql = ' '.join([
            'INSERT INTO post',
                '(id, html, publish_date, published)',
            'VALUES',
                '(:id, :html, :publish_date, :published)',
        ])
    values = {
        'id': md_meta['publishdate'],
        'html': html_string,
        'publish_date': md_meta['publishdate'],
        'published': md_meta['published'],
    }
    with con:
        con.execute(sql, values)

    # 中身の確認。
    rows = []
    for row in con.execute('SELECT * FROM post'):
        rows.append(dict(row))
    print(rows)
