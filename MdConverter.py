
"""MdConverter
### GitHub API 方式 ###
- font color が使えないので没。

### markdown 方式 ###
- 全体的にグッド。
- とくに attr_list がよい。
- pre タグに class をつけることができない。 これがなんとかなればこのライブラリで決定なんだが。
    - Allow modification of the class of the pre-tag in a fenced code block.
        - https://github.com/Python-Markdown/markdown/issues/506
    - Add support to specify table class with attr_list 
        - https://github.com/Python-Markdown/markdown/issues/312 

### markdown2 方式 ###
- pre タグに class をつけることができるが、一種類のみ。
- attr_list に相当するものがない。 それだけで2はダメダメ。

というわけで markdown を改造して使う方針がいいだろうか。

### Extension ###
- いつか自分で拡張を作ることになるかもしれない。
- https://python-markdown.github.io/reference/#output_format

"""

# 拡張のセットを変更できるように分離しておきます。
EXTENSIONS_ONE = [
    'fenced_code',  # ``` のコードを書ける。
    'tables',       # table を書ける。
    'def_list',     # dl dt dd を書ける。
    # 'nl2br',      # ふつーに改行すれば改行される。 まあこれはいらんかな。
    'sane_lists',   # デフォルトだとリストがバカだからなんとかする。
    'toc',          # [TOC] で目次自動生成。
    'attr_list',    # 要素に id とか class を追加できる。
    'markdown.extensions.smart_strong', # 強調がすこしだけバカだからなんとかする。
    'footnotes',    # 脚注が使えるようになる。
    'meta',         # メタ情報を MD 内に書ける。
]


import markdown

# MD ファイルから HTML ファイルを作成します。
def convert_md_file(md_path, html_path, exts):
    with open(md_path, 'r', encoding='utf8') as file:
        data = file.read()
    with open(html_path, 'w', encoding='utf8') as file:
        md_instance, html_string = create_md_instance(data, exts)
        file.write(html_string)

# MD インスタンス自体も Meta とか取得するのに有用だったので
# MD も html も返却するようにしました。
def create_md_instance(md_string, exts):
    md_instance = markdown.Markdown(extensions=exts)
    html_string = md_instance.convert(md_string)
    return md_instance, html_string


# ========================= <没になったメソッド> =========================

# GitHub API を用いて MD を HTML に変換する。 font color が使えない。
def convert_with_github_api(md_path, html_path):
    import urllib.request
    import urllib.parse
    with open(md_path, 'br') as file:
        data = file.read()
    request = urllib.request.Request('https://api.github.com/markdown/raw')
    request.add_header('Content-Type', 'text/plain')
    f = urllib.request.urlopen(request, data)
    with open(html_path, 'bw') as file:
        file.write(f.read())

# markdown2 ライブラリを用いて変換する。 attr_list がないので没。
def convert_md2(md_path, html_path):
    import markdown2
    with open(md_path, 'r', encoding='utf8') as file:
        data = file.read()
    with open(html_path, 'w', encoding='utf8') as file:
        # file.write(markdown2.markdown(data, extras=[
        #     'fenced-code-blocks',
        #     'tables',
        # ]))

        file.write(markdown2.markdown(data, extras={
            'fenced-code-blocks': None,
            'tables': None,
            'html-classes': {
                'pre': 'bg-light-color',
                'code': '',
                'table': '',
                'img': '',
            },
            'footnotes': None,
        }))

# ========================= </没になったメソッド> =========================

if __name__ == '__main__':
    # convert_with_github_api('./from.md', './to.html')
    convert_md_file('./from.md', './to.html', EXTENSIONS_ONE)
    # convert_md2('./from.md', './to.html')
